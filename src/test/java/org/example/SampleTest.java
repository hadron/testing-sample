package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SampleTest {
	Sample sample = new Sample();

	@Test
	void getDigSum() {
		Assertions.assertEquals(6, sample.getDigSum(123));
	}

	@Test
	void getDigSum2() {
		Assertions.assertEquals(21, sample.getDigSum(955011));
	}

	@Test
	void getDigSum3() {
		Assertions.assertEquals(8, sample.getDigSum(2123));
	}

	@Test
	void getDigProduct() {
		Assertions.assertEquals(6, sample.getDigProduct(123));
	}

	@Test
	void getDigProduct2() {
		Assertions.assertEquals(0, sample.getDigProduct(955011));
	}

	@Test
	void getDigProduct3() {
		Assertions.assertEquals(12, sample.getDigProduct(2123));
	}

	@Test
	void getConvertNum() {
		Assertions.assertEquals(321, sample.getConvertNum(123));
	}

	@Test
	void getConvertNum2() {
		Assertions.assertEquals(110559, sample.getConvertNum(955011));
	}

	@Test
	void getConvertNum3() {
		Assertions.assertEquals(302, sample.getConvertNum(2030));
	}


	@Test
	void relationPointCircle() {
		Assertions.assertEquals(-1, sample.relation_point_circle(-1, 2, 3));
	}

	@Test
	void relationPointCircle2() {
		Assertions.assertEquals(2, sample.relation_point_circle(5, 3, 4));
	}


	@Test
	void relationPointCircle3() {
		Assertions.assertEquals(3, sample.relation_point_circle(6, 6, 6));
	}

	@Test
	void relationPointCircle4() {
		Assertions.assertEquals(1, sample.relation_point_circle(10, 6, 6));
	}


	@Test
	void isSameCage() {
		Assertions.assertTrue(sample.isSameCage(35, 94));
	}

	@Test
	void isSameCage2() {
		Assertions.assertTrue(sample.isSameCage(2, 4));
	}

	@Test
	void isSameCage3() {
		Assertions.assertFalse(sample.isSameCage(4, 2));
	}

	@Test
	void isSameCage4() {
		Assertions.assertTrue(sample.isSameCage(10, 30));
	}

	@Test
	void getBuiedBottles() {
		Assertions.assertEquals(3, sample.getBuiedBottles(4));
	}

	@Test
	void getBuiedBottles1() {
		Assertions.assertEquals(4, sample.getBuiedBottles(5));
	}

	@Test
	void getBuiedBottles2() {
		Assertions.assertEquals(7, sample.getBuiedBottles(10));
	}

	@Test
	void isPalindromic() {
		Assertions.assertFalse(sample.isPalindromic(112));
	}

	@Test
	void isPalindromic2() {
		Assertions.assertTrue(sample.isPalindromic(959));
	}

	@Test
	void isPalindromic3() {
		Assertions.assertTrue(sample.isPalindromic(84348));
	}

	@Test
	void getRealPrice() {
		Assertions.assertEquals(4, sample.getRealPrice(5, 2));
	}

	@Test
	void getRealPrice2() {
		Assertions.assertEquals(0, sample.getRealPrice(1, 1));
	}

	@Test
	void getRealPrice3() {
		Assertions.assertEquals(77, sample.getRealPrice(150, 4));
	}

	@Test
	void getConstraintMax() {
		//  pdf是125
		Assertions.assertEquals(-1, sample.getConstraintMax(new int[]{5, 234, 56, 125, 983, 7}));
	}

	@Test
	void getConstraintMax2() {
		Assertions.assertEquals(-1, sample.getConstraintMax(new int[]{1, 34, 78, 1967, 337}));
	}

	@Test
	void getConstraintMax3() {
		Assertions.assertEquals(39210, sample.getConstraintMax(new int[]{39210, 235, 81, 92, 5, 3}));
	}

	@Test
	void getAppearanceIgnoreCase() {
		Assertions.assertEquals(5, sample.getAppearanceIgnoreCase("abAdFIJSHY,8912Abaca",'A'));
	}

	@Test
	void getAppearanceIgnoreCase2() {
		Assertions.assertEquals(0, sample.getAppearanceIgnoreCase("eirywotugqwyqir82372,\\?aic",'Z'));
	}

	@Test
	void getAppearanceIgnoreCase3() {
		//  pdf 里是3
		Assertions.assertEquals(4, sample.getAppearanceIgnoreCase("rty21opqtyTwets",'T'));
	}
}
