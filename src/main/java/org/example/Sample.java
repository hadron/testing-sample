package org.example;

public class Sample {

	/**
	 * 题目1：输入任意一个不超过6位的正整数 num，返回构成数字的和
	 */
	int getDigSum(int value) {
		String valueStr = String.valueOf(value);
		int result = 0;
		for (int i = 0; i < valueStr.length(); i++) {
			result += Integer.valueOf(String.valueOf(valueStr.charAt(i)));
		}
		return result;
	}

	/**
	 * 题目1：输入任意一个不超过6位的正整数 num，返回构成数字的乘积
	 */
	int getDigProduct(int value) {
		String valueStr = String.valueOf(value);
		int result = 0;
		for (int i = 0; i < valueStr.length(); i++) {
			int data = Integer.valueOf(String.valueOf(valueStr.charAt(i)));
			if (i == 0) {
				result = data;
			} else {
				result *= data;
			}
		}
		return result;
	}


	/**
	 * 题目2：输入任意一个不超过6位的正整数 num，返回由构成数字逆序组成的新整数
	 */
	int getConvertNum(int value) {
		String valueStr = String.valueOf(value);
		String newValue = "";
		for (int i = valueStr.length() - 1; i >= 0; i--) {
			newValue += String.valueOf(valueStr.charAt(i));
		}
		return Integer.valueOf(newValue);
	}

	/**
	 * 题目3：给定3个整数 r，x，y 表示圆的半径和一个点的 x，y 坐标，判断这个点和以（0，0）为圆心且半径为 r 的关系
	 */
	int relation_point_circle(int r, int x, int y) {
		if (r <= 0 || x <= 0 || y <= 0) {
			return -1;
		}
		double pointLength = Math.sqrt(Math.pow(Math.abs(x), 2) + Math.pow(Math.abs(y), 2));
		if (pointLength > r) {
			return 3;
		}
		if (pointLength == r) {
			return 2;
		}
		return 1;
	}

	/**
	 * 题目4：鸡兔同笼是否有解
	 */
	boolean isSameCage(int heads, int feet) {
		for (int i = 0; i <= heads; i++) {
			int r = (feet - i * 2) / 4;
			if (i + r == heads) {
				System.out.println("ji:" + i + " tuzi: " + r);
				return true;
			}
		}
		return false;
	}

	/**
	 * 题目5：为了倡导环保，景区进行售卖瓶装矿泉水促销活动
	 */
	int getBuiedBottles(int value) {
		int result = value;
		int remove = value / 3;
		return result - remove;
	}

	/**
	 * 题目6：给定任意一个不超过6位的正整数 x，判断是否为回文数
	 */
	boolean isPalindromic(int value) {
		String valueStr = String.valueOf(value);
		String newValue = "";
		for (int i = valueStr.length() - 1; i >= 0; i--) {
			newValue += String.valueOf(valueStr.charAt(i));
		}
		return valueStr.equals(newValue);
	}

	/**
	 * 题目7：购物促销
	 */
	int getRealPrice(int price, int count) {
		double rate = 1;
		if (count == 1) {
			rate = 0.95;
		} else if (count == 2) {
			rate = 0.9;
		} else if (count >= 3) {
			rate = 0.85;
		}
		double totalPrice = price * count * rate;
		if (totalPrice >= 200) {
			totalPrice -= 200;
		}
		return (int) totalPrice / count;
	}

	/**
	 * 题目8：给定一串正整数构成的序列，从中找到是3和5的倍数中最大的整数
	 */
	int getConstraintMax(int[] list) {
		int result = -1;
		for (int value : list) {
			if (value % 3 == 0 && value % 5 == 0) {
				if (value > result) {
					result = value;
				}
			}
		}
		return result;
	}

	/**
	 * 题目9：给定任意的一个字符串，在不区分大小写的情况下，统计给定字符重复出现的个数
	 */
	int getAppearanceIgnoreCase(String value, char c) {
		int result = 0;
		String v = String.valueOf(c);
		for (int i = 0; i < value.length(); i++) {
			String info = String.valueOf(value.charAt(i));
			if (info.equalsIgnoreCase(v)) {
				result++;
			}
		}
		return result;
	}
}
